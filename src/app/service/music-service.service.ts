import { Injectable } from '@angular/core';
import {default as exampledata} from '../../data/sample-music-data.json'
import { Artist } from '../models/Artist';
import { Album } from '../models/Album';
import { Track } from '../models/Track';

@Injectable({
  providedIn: 'root'
})
export class MusicServiceService {
  artists: Artist[] = []
  albums: Album[] = []

  constructor() {
    this.artists.push(new Artist(0, "The Beatles"));

    for(let x = 0; x < exampledata.length; x++){
      if(exampledata[x].artist == "The Beatles"){
        let tracks: Track[] = [];
        for(let y = 0; y < exampledata[x].tracks.length; y++){
          tracks.push(new Track(exampledata[x].tracks[y].id,exampledata[x].tracks[y].number, exampledata[x].tracks[y].title, exampledata[x].tracks[y].lyrics,""));
        }
        this.albums.push(new Album(exampledata[x].id, exampledata[x].title, exampledata[x].artist, exampledata[x].description, exampledata[x].year, exampledata[x].image, tracks));
      }
    }
   }

   getArtist(): Artist[]{
    return this.artists;
   }

   getAlbums(artist:string): Album[]{
     let albums: Album[] = [];
     for(let x = 0; x < this.albums.length; x++){
       if(this.albums[x].Artist == artist){
          albums.push( new Album(this.albums[x].Id,this.albums[x].Title, this.albums[x].Artist, this.albums[x].Description, this.albums[x].Year, this.albums[x].Image, this.albums[x].Tracks));
       }
     }
     return this.albums;
   }

   getAlbum(artist:string, id:number){
    for(let x = 0; x < this.albums.length; x++){
      if(this.albums[x].Artist == artist && this.albums[x].Id == id){
        return this.albums[x];
      }
    } 

    return null;
   }

   createAlbum(album:Album){
     if(this.albums.push(new Album(this.albums.length + 1, album.Title, album.Artist, album.Description, album.Year, album.Image, album.Tracks))){
       this.artists.push(new Artist(this.artists.length + 1, album.Artist));
       return this.albums.length;
     } else {
       return -1;
     }
   }

   updateAlbum(album:Album):number {
    for(let x = 0; x < this.albums.length; x++){
      if(this.albums[x].Id == album.Id){
        this.albums.splice(x, 1);
        this.albums.push(new Album(album.Id, album.Title, album.Artist, album.Description, album.Year, album.Image, album.Tracks));
        return 0;
      }
    }

    return -1;
   }

   deleteAlbum(id:number):number {
     for(let x = 0; x < this.albums.length; x++){
       if(this.albums[x].Id == id){
         this.albums.splice(x, 1);
         return 0;
       }
     }
     return -1;
   }
}
